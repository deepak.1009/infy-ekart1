#Schema for Infytel Ekart Customer
#==========================================================
create database infy_ekart_user;
use `infy_ekart_user`;

create table user(
user_email_id Varchar(50) primary key,
user_name Varchar(50),
user_password Varchar(50)
);


#Schema for Infytel Ekart Cards
#==========================================================
create database infy_ekart_cards;
use `infy_ekart_cards`;

create table cards(

card_number bigint NOT NULL,
name_on_card Varchar(50) NOT NULL,
expiry_month integer NOT NULL,
expiry_year integer NOT NULL,
user_id Varchar(50) NOT NULL,
PRIMARY KEY(card_number,user_id)
);


#Schema for Infytel Address
#============================================================

create database infy_ekart_address;
use `infy_ekart_address`;

create table address(

address_id integer NOT NULL AUTO_INCREMENT,
address Varchar(100) NOT NULL,
city Varchar(50) NOT NULL,
state Varchar(50) NOT NULL,
pincode integer NOT NULL,
phone_no bigint NOT NULL,
user_id Varchar(50) NOT NULL,

PRIMARY KEY(address_id),
UNIQUE KEY(address_id)
);

create table hibernate_sequence(
`next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into `hibernate_sequence` (`next_val`) Values(1);

create table states(
state_name Varchar(50)
);
INSERT INTO `states` (`state_name`) VALUES
('ANDHRA PRADESH'),
('ASSAM'),
('ARUNACHAL PRADESH'),
('GUJRAT'),
('BIHAR'),
('HARYANA'),
('HIMACHAL PRADESH'),
('JAMMU & KASHMIR'),
('KARNATAKA'),
('KERALA'),
('MADHYA PRADESH'),
('MAHARASHTRA'),
('MANIPUR'),
('MEGHALAYA'),
('MIZORAM'),
('NAGALAND'),
('ORISSA'),
('PUNJAB'),
('RAJASTHAN'),
('SIKKIM'),
('TAMIL NADU'),
('TRIPURA'),
('UTTAR PRADESH'),
('WEST BENGAL'),
('DELHI'),
('GOA'),
('PONDICHERY'),
('LAKSHDWEEP'),
('DAMAN & DIU'),
('DADRA & NAGAR'),
('CHANDIGARH'),
('ANDAMAN & NICOBAR'),
('UTTARANCHAL'),
('JHARKHAND'),
('CHATTISGARH');