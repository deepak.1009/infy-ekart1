package com.infy.ekart.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.infy.ekart.user.entity.UserEntity;


public interface UserRepository extends JpaRepository<UserEntity, String> {

	@Query(value="Select * from user u where user_email_id=?1",nativeQuery = true)
	public UserEntity findByEmailId(String emailId);
}
