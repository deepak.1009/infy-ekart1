package com.infy.ekart.user.exceptions;

public class InvalidUserPasswordException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidUserPasswordException() {
		super();
	}
	
	public InvalidUserPasswordException(String error) {
		super(error);
	}
}
