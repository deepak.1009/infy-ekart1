package com.infy.ekart.user.exceptions;

public class LoginAuthenticationException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public LoginAuthenticationException() {
		super();
	}
	
	public LoginAuthenticationException(String error) {
		super(error);
	}

}
