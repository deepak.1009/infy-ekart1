package com.infy.ekart.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="user")
public class UserEntity {

	@Id
	@Column(name="user_email_id", nullable=false,length=50)
	String userId;
	
	@Column(name="user_name", nullable=false,length=50)
	String userName;
	
	@Column(name="user_password", nullable=false, length=50)
	String userPassword;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public UserEntity() {
		super();
	}
	
	
}
