package com.infy.ekart.user.dto;

public class UpdateUserDetailsDTO {

	String userName;
	String oldPassword;
	String newPassword;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public UpdateUserDetailsDTO() {
		super();
	}
	
	
}
