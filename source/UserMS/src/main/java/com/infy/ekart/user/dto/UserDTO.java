package com.infy.ekart.user.dto;

import com.infy.ekart.user.entity.UserEntity;

public class UserDTO {

	String userEmailId;
	String userName;
	String password;
	
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public static UserDTO valueOf(UserEntity user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setUserEmailId(user.getUserId());
		userDTO.setUserName(user.getUserName());
		userDTO.setPassword(user.getUserPassword());
		return userDTO;
	}
	
	public UserEntity createEntity() {
		UserEntity user = new UserEntity();
		user.setUserId(this.getUserEmailId().trim());
		user.setUserName(this.getUserName().trim());
		user.setUserPassword(this.getPassword().trim());
	
		return user;
	}
}
