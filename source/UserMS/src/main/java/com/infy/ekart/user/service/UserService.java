package com.infy.ekart.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.ekart.user.dto.LoginDTO;
import com.infy.ekart.user.dto.UpdateUserDetailsDTO;
import com.infy.ekart.user.dto.UserDTO;
import com.infy.ekart.user.entity.UserEntity;
import com.infy.ekart.user.exceptions.EmailIdAlreadyInUseException;
import com.infy.ekart.user.exceptions.InvalidOldPasswordException;
import com.infy.ekart.user.exceptions.InvalidUserIdException;
import com.infy.ekart.user.exceptions.InvalidUserNameException;
import com.infy.ekart.user.exceptions.InvalidUserPasswordException;
import com.infy.ekart.user.exceptions.LoginAuthenticationException;
import com.infy.ekart.user.repository.UserRepository;
import com.infy.ekart.user.validator.EmailValidator;
import com.infy.ekart.user.validator.PasswordValidator;
import com.infy.ekart.user.validator.UserNameValidator;

@Service
public class UserService {

	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	EmailValidator emailValidator;
	
	@Autowired
	UserNameValidator usernameValidator;
	
	@Autowired
	PasswordValidator passwordValidator;
	
	public boolean login(LoginDTO login){
		
		UserEntity requestedUser = userRepo.findByEmailId(login.getUserId());
		
		if(requestedUser!=null){
			if(requestedUser.getUserPassword().equals(login.getPassword())) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean userIdExistCheck(String userId) {
		UserEntity requestedUser = userRepo.findByEmailId(userId);
		return (requestedUser!=null);
	}
	
	public void newUserSignUp(UserDTO userDTO) throws Exception{
		
		UserEntity requestedUser = userRepo.findByEmailId(userDTO.getUserEmailId());
		
		if(requestedUser!=null) {
			throw new EmailIdAlreadyInUseException();
		}
		
		if(!emailValidator.isValidEmail(userDTO.getUserEmailId())) {
			throw new InvalidUserIdException();
		}
		
		if(!usernameValidator.isUserNameValid(userDTO.getUserName())) {
			throw new InvalidUserNameException();
		}
		
		if(!passwordValidator.isPasswordValid(userDTO.getPassword())) {
			throw new InvalidUserPasswordException();
		}
		
		UserEntity user = userDTO.createEntity();
		userRepo.saveAndFlush(user);
		
	}
	
	public void updateUserDetails(UpdateUserDetailsDTO updateDetails, String userId) throws Exception{
		
		LoginDTO authenticate = new LoginDTO();
		authenticate.setUserId(userId);
		authenticate.setPassword(updateDetails.getOldPassword());
		
		boolean validLogin = this.login(authenticate);
		
		if(!validLogin) {
			throw new LoginAuthenticationException();
		}
		
		if(updateDetails.getOldPassword().equals(updateDetails.getNewPassword())) {
			throw new InvalidOldPasswordException();
		}
		
		if(!usernameValidator.isUserNameValid(updateDetails.getUserName())) {
			throw new InvalidUserNameException();
		}
		
		if(!passwordValidator.isPasswordValid(updateDetails.getNewPassword())) {
			throw new InvalidUserPasswordException();
		}
		
		UserEntity updatedUser = new UserEntity();
		updatedUser.setUserId(userId);
		updatedUser.setUserName(updateDetails.getUserName());
		updatedUser.setUserPassword(updateDetails.getNewPassword());
		
		userRepo.save(updatedUser);
	}
	
	
}
