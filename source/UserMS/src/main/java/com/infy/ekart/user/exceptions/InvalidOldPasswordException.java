package com.infy.ekart.user.exceptions;

public class InvalidOldPasswordException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidOldPasswordException() {
		super();
	}
	
	public InvalidOldPasswordException(String error) {
		super(error);
	}
}
