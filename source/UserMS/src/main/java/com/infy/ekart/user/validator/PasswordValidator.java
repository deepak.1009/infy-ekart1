package com.infy.ekart.user.validator;

import org.springframework.stereotype.Component;

@Component
public class PasswordValidator {

	public boolean isPasswordValid(String password) {
		
		if(password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$")) {
			return true;
		}
		
		return false;
	}
}
