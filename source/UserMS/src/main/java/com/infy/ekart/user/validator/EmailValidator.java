package com.infy.ekart.user.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class EmailValidator {

		
		
		private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

		private static Pattern pattern;

		private Matcher matcher;

		public EmailValidator() {
			// initialize the Pattern object
			pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
		}

		public boolean isValidEmail(String email) {
			matcher = pattern.matcher(email);
			return matcher.matches();
		}
}

