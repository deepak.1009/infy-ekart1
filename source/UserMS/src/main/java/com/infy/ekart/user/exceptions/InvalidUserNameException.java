package com.infy.ekart.user.exceptions;

public class InvalidUserNameException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidUserNameException() {
		super();
	}
	
	public InvalidUserNameException(String error) {
		super(error);
	}
}
