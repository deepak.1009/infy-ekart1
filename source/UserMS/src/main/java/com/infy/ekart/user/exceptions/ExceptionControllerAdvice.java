package com.infy.ekart.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.infy.ekart.user.dto.ErrorMessage;

@RestControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(Exception.class)
	public String  exceptionHandler(Exception ex) {
		 
		return  ex.getMessage();
	}
	
	@ExceptionHandler(LoginAuthenticationException.class)
	public ResponseEntity<ErrorMessage> exceptionHandler(LoginAuthenticationException ex) {
		 ErrorMessage error = new ErrorMessage();
	        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
	        error.setMessage("Invalid userid or/and password, please try again with valid combination of userid and password");
	        return new ResponseEntity<>(error, HttpStatus.OK);
		 
	}
	
	@ExceptionHandler(EmailIdAlreadyInUseException.class)
	public ResponseEntity<ErrorMessage> emailIdAlreadyInUseHandler(EmailIdAlreadyInUseException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid userid, email id already in use");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidUserIdException.class)
	public ResponseEntity<ErrorMessage> invalidUserIdHandler(InvalidUserIdException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid userid, it should be in format <anything>@<anything>.<anything>");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidUserNameException.class)
	public ResponseEntity<ErrorMessage> invalidUserNameHandler(InvalidUserNameException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid username, it should not contain any numeric or/and special characters and it should not be blank");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidUserPasswordException.class)
	public ResponseEntity<ErrorMessage> invalidUserPasswordHandler(InvalidUserPasswordException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid user password, Password must be of length 8 to 20 and contain at least an uppercase and a lowercase character, a number and a special character");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidOldPasswordException.class)
	public ResponseEntity<ErrorMessage> invalidOldPasswrod(InvalidOldPasswordException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid password, new password and old password must be different");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
}
