package com.infy.ekart.user.exceptions;

public class EmailIdAlreadyInUseException extends Exception{

private static final long serialVersionUID = 1L;
	
	public EmailIdAlreadyInUseException() {
		super();
	}
	
	public EmailIdAlreadyInUseException(String error) {
		super(error);
	}
}
