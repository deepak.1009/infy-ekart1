package com.infy.ekart.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.infy.ekart.user.dto.LoginDTO;
import com.infy.ekart.user.dto.UpdateUserDetailsDTO;
import com.infy.ekart.user.dto.UserDTO;
import com.infy.ekart.user.exceptions.LoginAuthenticationException;
import com.infy.ekart.user.service.UserService;

@RestController
@CrossOrigin
public class UserController {

	@Autowired
	UserService userService;
	
	
	@PostMapping(value="/login")
	public ResponseEntity<String> login(@RequestBody LoginDTO loginDTO) throws LoginAuthenticationException{
		Boolean validLogin = userService.login(loginDTO);
		if(!validLogin) {
			throw new LoginAuthenticationException();
		}
		String response = "Login Successful. Welcome "+loginDTO.getUserId();
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value="{userid}/searchUser")
	public boolean checkUserIdExist(@PathVariable String userid) {
		return userService.userIdExistCheck(userid);
	}
	
	@PostMapping(value="/signup")
	public ResponseEntity<String> signUp(@RequestBody UserDTO userDTO) throws Exception{
		userService.newUserSignUp(userDTO);
		String response = "User created successfully!!!";
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(value="/{userId}/update")
	public ResponseEntity<String> updateUserDetails(@PathVariable String userId,@RequestBody UpdateUserDetailsDTO updateUserDetails) throws Exception{
		userService.updateUserDetails(updateUserDetails, userId);
		String response = "Username and password updated successfully!!!";
		return ResponseEntity.ok(response);
	}
}
