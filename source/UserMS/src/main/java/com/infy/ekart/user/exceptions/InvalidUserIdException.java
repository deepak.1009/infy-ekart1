package com.infy.ekart.user.exceptions;

public class InvalidUserIdException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidUserIdException() {
		super();
	}
	
	public InvalidUserIdException(String error) {
		super(error);
	}
}
