package com.infy.ekart.user.validator;

import org.springframework.stereotype.Component;

@Component
public class UserNameValidator {

	public boolean isUserNameValid(String username) {
		
		if (username.matches("^(?![\\s.]+$)[a-zA-Z\\s]*$") && !username.trim().isEmpty()) {
			return true;
		}
		
		return false;
	}
	
}
