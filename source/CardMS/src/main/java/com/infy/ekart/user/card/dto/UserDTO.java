package com.infy.ekart.user.card.dto;

import java.util.List;

import com.infy.ekart.user.card.entity.CardEntity;

public class UserDTO {

	String userId;
	String password;
	List<CardEntity> cardList;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<CardEntity> getCardList() {
		return cardList;
	}
	public void setCardList(List<CardEntity> cardList) {
		this.cardList = cardList;
	}
	
	
}
