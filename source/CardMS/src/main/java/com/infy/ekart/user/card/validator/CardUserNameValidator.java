package com.infy.ekart.user.card.validator;

import org.springframework.stereotype.Component;

@Component
public class CardUserNameValidator {

	public boolean isCardUserNameValid(String username) {
		
		if (username.matches("^(?![\\s.]+$)[a-zA-Z\\s]*$") && !username.trim().isEmpty()) {
			return true;
		}
		
		return false;
	}
	
}
