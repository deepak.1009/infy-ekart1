package com.infy.ekart.user.card.exceptions;

public class CardAlreadyExistForUserException extends Exception{

private static final long serialVersionUID = 1L;
	
	public CardAlreadyExistForUserException() {
		super();
	}
	
	public CardAlreadyExistForUserException(String error) {
		super(error);
	}
}
