package com.infy.ekart.user.card.exceptions;

public class CardDoesNotExistForUserException extends Exception{

private static final long serialVersionUID = 1L;
	
	public CardDoesNotExistForUserException() {
		super();
	}
	
	public CardDoesNotExistForUserException(String error) {
		super(error);
	}
}
