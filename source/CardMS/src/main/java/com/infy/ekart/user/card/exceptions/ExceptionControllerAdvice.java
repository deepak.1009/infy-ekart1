package com.infy.ekart.user.card.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.infy.ekart.user.card.dto.ErrorMessage;


@RestControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(Exception.class)
	public String  exceptionHandler(Exception ex) {
		 
		return  ex.getMessage();
	}
	
	@ExceptionHandler(InvalidCardNumberException.class)
	public ResponseEntity<ErrorMessage> exceptionHandler(InvalidCardNumberException ex) {
		 ErrorMessage error = new ErrorMessage();
	        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
	        error.setMessage("Invalid card number! Card number must be of 16 digits");
	        return new ResponseEntity<>(error, HttpStatus.OK);
		 
	}
	
	@ExceptionHandler(InvalidCardExpiryMonthException.class)
	public ResponseEntity<ErrorMessage> emailIdAlreadyInUseHandler(InvalidCardExpiryMonthException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid expiry month, must be between 1 to 12");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidCardExpiryYearException.class)
	public ResponseEntity<ErrorMessage> invalidUserIdHandler(InvalidCardExpiryYearException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid expiry year, must be between 21 to 35");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidCardUserNameException.class)
	public ResponseEntity<ErrorMessage> invalidUserNameHandler(InvalidCardUserNameException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid card user name, it should not contain any numeric or/and special characters and it should not be blank");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(CardAlreadyExistForUserException.class)
	public ResponseEntity<ErrorMessage> cardAlreadyExistForUser(CardAlreadyExistForUserException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Card already exist for user");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(CardDoesNotExistForUserException.class)
	public ResponseEntity<ErrorMessage> cardDoesNotExistForUser(CardDoesNotExistForUserException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Card does not exist for user");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(UserIdDoesNotExistException.class)
	public ResponseEntity<ErrorMessage> userIdDoesNotExist(UserIdDoesNotExistException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Userid does not exist!!! please check the url");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
}
