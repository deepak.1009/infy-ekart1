package com.infy.ekart.user.card.exceptions;

public class UserIdDoesNotExistException extends Exception{

private static final long serialVersionUID = 1L;
	
	public UserIdDoesNotExistException() {
		super();
	}
	
	public UserIdDoesNotExistException(String error) {
		super(error);
	}
}
