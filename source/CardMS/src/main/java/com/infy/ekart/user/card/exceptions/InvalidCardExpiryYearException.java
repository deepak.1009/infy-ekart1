package com.infy.ekart.user.card.exceptions;

public class InvalidCardExpiryYearException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidCardExpiryYearException() {
		super();
	}
	
	public InvalidCardExpiryYearException(String error) {
		super(error);
	}
}
