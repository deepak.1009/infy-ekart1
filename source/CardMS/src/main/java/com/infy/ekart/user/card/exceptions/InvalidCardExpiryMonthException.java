package com.infy.ekart.user.card.exceptions;

public class InvalidCardExpiryMonthException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidCardExpiryMonthException() {
		super();
	}
	
	public InvalidCardExpiryMonthException(String error) {
		super(error);
	}
}
