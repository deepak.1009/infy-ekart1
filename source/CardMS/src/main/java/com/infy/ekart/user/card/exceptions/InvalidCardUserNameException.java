package com.infy.ekart.user.card.exceptions;

public class InvalidCardUserNameException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidCardUserNameException() {
		super();
	}
	
	public InvalidCardUserNameException(String error) {
		super(error);
	}
}
