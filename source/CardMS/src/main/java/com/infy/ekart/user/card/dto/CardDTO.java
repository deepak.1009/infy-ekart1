package com.infy.ekart.user.card.dto;

import com.infy.ekart.user.card.entity.CardEntity;
import com.infy.ekart.user.card.entity.CardID;

public class CardDTO {

	CardID cardId;
	String nameOnCard;
	Integer expiryMonth;
	Integer expiryYear;
	
	
	public CardID getCardId() {
		return cardId;
	}
	public void setCardId(CardID cardId) {
		this.cardId = cardId;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public Integer getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(Integer expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public Integer getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
	}
	
	public static CardDTO valueOf(CardEntity card) {
		CardDTO cardDTO = new CardDTO();
		cardDTO.setCardId(card.getCardId());
		cardDTO.setNameOnCard(card.getNameOnCard());
		cardDTO.setExpiryMonth(card.getExpiryMonth());
		cardDTO.setExpiryYear(card.getExpiryYear());
		return cardDTO;
	}
	
	public CardEntity createEntity() {
		CardEntity card = new CardEntity();
		card.setCardId(this.getCardId());
		card.setNameOnCard(this.getNameOnCard());
		card.setExpiryMonth(this.getExpiryMonth());
		card.setExpiryYear(this.getExpiryYear());
	
		return card;
	}
}
