package com.infy.ekart.user.card.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import org.hibernate.validator.constraints.Range;

@Entity(name="cards")
public class CardEntity {

	@EmbeddedId
	private CardID cardId;
	
	@Column(name="name_on_card")
	private String nameOnCard;
	
	@Column(name="expiry_month")
	@Range(min=1, max=12)
	private Integer expiryMonth;
	
	@Column(name="expiry_year")
	@Range(min=21, max=35)
	private Integer expiryYear;

	public CardEntity() {
		super();
	}

	public CardID getCardId() {
		return cardId;
	}

	public void setCardId(CardID cardId) {
		this.cardId = cardId;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public Integer getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(Integer expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public Integer getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
	}

	
	
	
}
