package com.infy.ekart.user.card.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.ekart.user.card.dto.CardToDisplay;
import com.infy.ekart.user.card.entity.CardEntity;
import com.infy.ekart.user.card.entity.CardID;
import com.infy.ekart.user.card.exceptions.CardAlreadyExistForUserException;
import com.infy.ekart.user.card.exceptions.CardDoesNotExistForUserException;
import com.infy.ekart.user.card.exceptions.InvalidCardExpiryMonthException;
import com.infy.ekart.user.card.exceptions.InvalidCardExpiryYearException;
import com.infy.ekart.user.card.exceptions.InvalidCardNumberException;
import com.infy.ekart.user.card.exceptions.InvalidCardUserNameException;
import com.infy.ekart.user.card.repository.CardRepository;
import com.infy.ekart.user.card.validator.CardUserNameValidator;

@Service
public class CardService {

	@Autowired
	CardRepository cardRepository;
	
	@Autowired
	CardUserNameValidator cardUserNameValidator;
	
	public List<CardToDisplay> getCardsForUser(String userId){
		
		List<CardEntity> fetchFromDB= cardRepository.findByUserId(userId);
		List<CardToDisplay> returnable = new ArrayList<CardToDisplay>();
		
		for(CardEntity card:fetchFromDB) {
			CardToDisplay cardToDisplay = new CardToDisplay();
			cardToDisplay.setCardNumber(card.getCardId().getCardNumber());
			cardToDisplay.setNameOnCard(card.getNameOnCard());
			cardToDisplay.setExpiryMonth(card.getExpiryMonth());
			cardToDisplay.setExpiryYear(card.getExpiryYear());
			returnable.add(cardToDisplay);
		}
		
		return returnable;
	}
	
	public void addCardForExistingCustomer(CardToDisplay card, String userId) throws Exception{
		
		try{
			
		CardEntity checkDuplicate = cardRepository.checkForDuplicate(userId, card.getCardNumber());
		if(checkDuplicate!=null) {
			throw new CardAlreadyExistForUserException("card already exists");
		}
		
		if(String.valueOf(card.getCardNumber()).length()!=16) {
			throw new InvalidCardNumberException("invalid card number");
		}
		if(!cardUserNameValidator.isCardUserNameValid(card.getNameOnCard())) {
			throw new InvalidCardUserNameException("invalid card user name");
		}
		CardEntity cardToAdd = new CardEntity();
		
		CardID cardId = new CardID();
		cardId.setCardNumber(card.getCardNumber());
		cardId.setUserId(userId);
		
		cardToAdd.setCardId(cardId);
		cardToAdd.setExpiryMonth(card.getExpiryMonth());
		cardToAdd.setExpiryYear(card.getExpiryYear());
		cardToAdd.setNameOnCard(card.getNameOnCard());
		
		cardRepository.saveAndFlush(cardToAdd);
		}
		catch(Exception e){
			if(e.getMessage().toLowerCase().contains("expirymonth")) {
				throw new InvalidCardExpiryMonthException();
			}
			if(e.getMessage().toLowerCase().contains("expiryyear")) {
				throw new InvalidCardExpiryYearException();
			}
			else {
				throw e;
			}
		}
		
	}
	
	public void deleteCardForCustomer(String userId, Long cardNumber) throws Exception{
		
		CardEntity cardToDelete = cardRepository.checkForDuplicate(userId, cardNumber);
		if(cardToDelete==null) {
			throw new CardDoesNotExistForUserException();
		}
		cardRepository.delete(cardToDelete);
		
	}
}
