package com.infy.ekart.user.card.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.infy.ekart.user.card.entity.CardEntity;
import com.infy.ekart.user.card.entity.CardID;

public interface CardRepository extends JpaRepository<CardEntity, CardID>{

	@Query(value="Select * from cards c where user_id=?1",nativeQuery = true)
	public List<CardEntity> findByUserId(String userId);
	
	@Query(value="Select * from cards c where user_id=?1 and card_number=?2",nativeQuery = true)
	public CardEntity checkForDuplicate(String userId, Long cardNumber);
	
}
