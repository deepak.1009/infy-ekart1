package com.infy.ekart.user.card.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CardID implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long cardNumber;
	private String userId;
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
