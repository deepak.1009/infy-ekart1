package com.infy.ekart.user.card.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.infy.ekart.user.card.dto.CardToDisplay;
import com.infy.ekart.user.card.exceptions.UserIdDoesNotExistException;
import com.infy.ekart.user.card.service.CardService;

@RestController
@CrossOrigin
public class CardsController {

	
	@Autowired
	CardService cardService;
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping(value="/{userid}/cards")
	public List<CardToDisplay> getCardsForUser(@PathVariable String userid) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		return cardService.getCardsForUser(userid);
	}
	
	@PostMapping(value="/{userid}/card/add")
	public ResponseEntity<String> addCardForUser(@PathVariable String userid, @RequestBody CardToDisplay cardToDisplay) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		cardService.addCardForExistingCustomer(cardToDisplay, userid);
		String response = "Card added successfully!!!";
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(value="/{userid}/card/{cardnumber}/delete")
	public ResponseEntity<String> deleteCardForUser(@PathVariable String userid, @PathVariable Long cardnumber) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		cardService.deleteCardForCustomer(userid, cardnumber);
		String response = "Card deleted successfully!!!";
		return ResponseEntity.ok(response);
	}
}
