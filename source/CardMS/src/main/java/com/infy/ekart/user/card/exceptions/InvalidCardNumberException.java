package com.infy.ekart.user.card.exceptions;

public class InvalidCardNumberException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidCardNumberException() {
		super();
	}
	
	public InvalidCardNumberException(String error) {
		super(error);
	}
}
