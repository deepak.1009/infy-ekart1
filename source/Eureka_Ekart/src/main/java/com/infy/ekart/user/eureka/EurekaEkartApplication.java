package com.infy.ekart.user.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaEkartApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaEkartApplication.class, args);
	}

}
