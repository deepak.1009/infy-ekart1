package com.infy.ekart.user.address.exceptions;

public class InvalidAddressIdException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidAddressIdException() {
		super();
	}
	
	public InvalidAddressIdException(String error) {
		super(error);
	}
}
