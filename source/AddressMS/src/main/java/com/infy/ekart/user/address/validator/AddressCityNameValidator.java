package com.infy.ekart.user.address.validator;

import org.springframework.stereotype.Component;

@Component
public class AddressCityNameValidator {

	public boolean isAddressCityNameValid(String cityname) {
		
		if (cityname.matches("^(?![\\s.]+$)[a-zA-Z\\s]*$") && !cityname.trim().isEmpty()) {
			return true;
		}
		
		return false;
	}
	
}
