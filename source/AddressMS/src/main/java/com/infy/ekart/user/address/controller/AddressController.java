package com.infy.ekart.user.address.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.infy.ekart.user.address.Service.AddressService;
import com.infy.ekart.user.address.dto.AddressDTO;
import com.infy.ekart.user.address.dto.AddressToDisplay;
import com.infy.ekart.user.address.exceptions.UserIdDoesNotExistException;

@RestController
@CrossOrigin
public class AddressController {

	
	@Autowired
	AddressService addressService;
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping(value="/{userid}/address/{addressid}/view")
	public AddressToDisplay getAddressById(@PathVariable String userid, @PathVariable Integer addressid) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		return addressService.viewAddressById(userid, addressid);
	}
	
	@GetMapping(value="/{userid}/address/view")
	public List<AddressToDisplay> getAddressesByUserId(@PathVariable String userid) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		return addressService.viewAddressesByUserId(userid);
	}
	
	@PostMapping(value="/{userid}/address/add")
	public ResponseEntity<String> addAddress(@PathVariable String userid, @RequestBody AddressDTO addressDTO) throws Exception {
		
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		addressService.addAddressForUser(userid, addressDTO);
		String response = "Address added successfully!!!";
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(value="/{userid}/address/{addressid}/modify")
	public ResponseEntity<String> modifyAddress(@PathVariable String userid,@PathVariable Integer addressid, @RequestBody AddressDTO addressDTO) throws Exception {
		
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		addressService.modifyAddress(userid, addressid, addressDTO);
		String response = "Address updated successfully!!!";
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(value="/{userid}/address/{addressid}/delete")
	public ResponseEntity<String> deleteAddress(@PathVariable String userid, @PathVariable Integer addressid) throws Exception{
		boolean validUser = restTemplate.getForObject("http://USERMS/"+userid+"/searchUser", boolean.class);
		if(!validUser) {
			throw new UserIdDoesNotExistException();
		}
		addressService.deleteAddress(userid, addressid);
		String response = "Address deleted successfully!!!";
		return ResponseEntity.ok(response);
	}
}
