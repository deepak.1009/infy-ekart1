package com.infy.ekart.user.address.exceptions;

public class InvalidStateNameException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidStateNameException() {
		super();
	}
	
	public InvalidStateNameException(String error) {
		super(error);
	}
}
