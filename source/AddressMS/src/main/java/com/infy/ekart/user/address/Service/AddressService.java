package com.infy.ekart.user.address.Service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.ekart.user.address.dto.AddressDTO;
import com.infy.ekart.user.address.dto.AddressToDisplay;
import com.infy.ekart.user.address.entity.AddressEntity;
import com.infy.ekart.user.address.exceptions.InvalidAddressIdException;
import com.infy.ekart.user.address.exceptions.InvalidCityNameException;
import com.infy.ekart.user.address.exceptions.InvalidPhoneNumberException;
import com.infy.ekart.user.address.exceptions.InvalidPinCodeException;
import com.infy.ekart.user.address.exceptions.InvalidStateNameException;
import com.infy.ekart.user.address.repository.AddressRepository;
import com.infy.ekart.user.address.repository.StateRepository;
import com.infy.ekart.user.address.validator.AddressCityNameValidator;

@Service
public class AddressService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	AddressRepository addressRepo;
	
	@Autowired
	StateRepository stateRepo;
	
	@Autowired
	AddressCityNameValidator addressCityNameValidator;
	
	public void addAddressForUser(String userId, AddressDTO addressDTO) throws Exception{
		
		try {
		List<String> validStates = stateRepo.getValidStates();
		if(!validStates.stream().anyMatch(addressDTO.getState().trim()::equalsIgnoreCase)) {
			throw new InvalidStateNameException(String.join(",", validStates));
		}
		
		if(!addressCityNameValidator.isAddressCityNameValid(addressDTO.getCity())) {
			throw new InvalidCityNameException("invalid city name");
		}
		AddressEntity address = addressDTO.createAddress(userId);
		addressRepo.saveAndFlush(address);
		
		}
		catch(Exception e) {
			logger.info(e.getMessage());
			if(e.getMessage().toLowerCase().contains("pincode")) {
				throw new InvalidPinCodeException();
			}
			if(e.getMessage().toLowerCase().contains("phoneno")) {
				throw new InvalidPhoneNumberException();
			}
			else {
				throw e;
			}
		}
	}
	
	public void deleteAddress(String userId,Integer addressId) throws Exception{
		
		AddressEntity fetchedAdderess = addressRepo.getByUserAddressId(addressId, userId);
		if(fetchedAdderess==null) {
			throw new InvalidAddressIdException(userId);
		}
		addressRepo.deleteById(addressId);
	}
	
	public void modifyAddress(String userId,Integer addressId, AddressDTO addressDTO) throws Exception{
		try {
			List<String> validStates = stateRepo.getValidStates();
			if(!validStates.stream().anyMatch(addressDTO.getState().trim()::equalsIgnoreCase)) {
				throw new InvalidStateNameException(String.join(",", validStates));
			}
			
			if(!addressCityNameValidator.isAddressCityNameValid(addressDTO.getCity())) {
				throw new InvalidCityNameException("invalid city name");
			}
			AddressEntity fetchedAdderess = addressRepo.getByUserAddressId(addressId, userId);
			if(fetchedAdderess==null) {
				throw new InvalidAddressIdException(userId);
			}
			AddressEntity address = addressDTO.createAddress(userId);
			address.setAddressId(addressId);
			addressRepo.saveAndFlush(address);
			
			}
			catch(Exception e) {
				logger.info(e.getMessage());
				if(e.getMessage().toLowerCase().contains("pincode")) {
					throw new InvalidPinCodeException();
				}
				if(e.getMessage().toLowerCase().contains("phoneno")) {
					throw new InvalidPhoneNumberException();
				}
				else {
					throw e;
				}
			}
	}
	
	public AddressToDisplay viewAddressById(String userId, Integer addressId) throws Exception{
		
		AddressEntity fetchedAdderess = addressRepo.getByUserAddressId(addressId, userId);
		if(fetchedAdderess==null) {
			throw new InvalidAddressIdException(userId);
		}
		
		AddressToDisplay addressToDisplay = new AddressToDisplay();
		addressToDisplay.setAddressId(addressId);
		addressToDisplay.setAddress(fetchedAdderess.getAddress());
		addressToDisplay.setCity(fetchedAdderess.getCity());
		addressToDisplay.setState(fetchedAdderess.getState());
		addressToDisplay.setPincode(fetchedAdderess.getPincode());
		addressToDisplay.setPhoneNo(fetchedAdderess.getPhoneNo());
		
		return addressToDisplay;
	}
	
public List<AddressToDisplay> viewAddressesByUserId(String userId) throws Exception{
		
		List<AddressEntity> fetchedAdderesses = addressRepo.getAddresses(userId);
		
		List<AddressToDisplay> returnable = new ArrayList<AddressToDisplay>();
		
		for(AddressEntity address:fetchedAdderesses) {
		
		AddressToDisplay addressToDisplay = new AddressToDisplay();
		addressToDisplay.setAddressId(address.getAddressId());
		addressToDisplay.setAddress(address.getAddress());
		addressToDisplay.setCity(address.getCity());
		addressToDisplay.setState(address.getState());
		addressToDisplay.setPincode(address.getPincode());
		addressToDisplay.setPhoneNo(address.getPhoneNo());
		
		returnable.add(addressToDisplay);
		
		}
		return returnable;
	}
}
