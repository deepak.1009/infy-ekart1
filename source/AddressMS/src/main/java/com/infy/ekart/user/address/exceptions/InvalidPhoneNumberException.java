package com.infy.ekart.user.address.exceptions;

public class InvalidPhoneNumberException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidPhoneNumberException() {
		super();
	}
	
	public InvalidPhoneNumberException(String error) {
		super(error);
	}
}
