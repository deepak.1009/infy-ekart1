package com.infy.ekart.user.address.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.infy.ekart.user.address.entity.AddressEntity;


public interface AddressRepository extends JpaRepository<AddressEntity, Integer>{

	@Query(value="SELECT * from address a where address_id=?1 and user_id=?2",nativeQuery= true)
	public AddressEntity getByUserAddressId(Integer addressId, String userId);
	
	@Query(value="SELECT * from address a where user_id=?1",nativeQuery= true)
	public List<AddressEntity> getAddresses(String userId);
}
