package com.infy.ekart.user.address.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.infy.ekart.user.address.entity.StateEntity;


public interface StateRepository extends JpaRepository<StateEntity, String>{

	@Query(value="Select * from states",nativeQuery = true)
	public List<String> getValidStates();
	
}
