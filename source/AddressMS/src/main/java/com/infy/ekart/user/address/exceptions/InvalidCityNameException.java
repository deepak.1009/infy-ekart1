package com.infy.ekart.user.address.exceptions;

public class InvalidCityNameException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidCityNameException() {
		super();
	}
	
	public InvalidCityNameException(String error) {
		super(error);
	}
}
