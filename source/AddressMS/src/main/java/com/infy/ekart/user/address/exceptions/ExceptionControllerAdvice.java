package com.infy.ekart.user.address.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.infy.ekart.user.address.dto.ErrorMessage;



@RestControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(Exception.class)
	public String  exceptionHandler(Exception ex) {
		 
		return  ex.getMessage();
	}
	
	@ExceptionHandler(InvalidPhoneNumberException.class)
	public ResponseEntity<ErrorMessage> invalidPhoneNumber(InvalidPhoneNumberException ex) {
		 ErrorMessage error = new ErrorMessage();
	        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
	        error.setMessage("Invalid phone number! Phone number must be of 10 digits");
	        return new ResponseEntity<>(error, HttpStatus.OK);
		 
	}
	
	@ExceptionHandler(InvalidPinCodeException.class)
	public ResponseEntity<ErrorMessage> invalidPincode(InvalidPinCodeException ex) {
		 ErrorMessage error = new ErrorMessage();
	        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
	        error.setMessage("Invalid pincode! Pincode must be of 6 digits");
	        return new ResponseEntity<>(error, HttpStatus.OK);
		 
	}
	
	
	@ExceptionHandler(InvalidCityNameException.class)
	public ResponseEntity<ErrorMessage> invalidCityNameHandler(InvalidCityNameException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid city name, it should not contain any numeric or/and special characters and it should not be blank");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidStateNameException.class)
	public ResponseEntity<ErrorMessage> invalidCityNameHandler(InvalidStateNameException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Invalid state name, it should be any state from the given list: "+ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(UserIdDoesNotExistException.class)
	public ResponseEntity<ErrorMessage> userIdDoesNotExist(UserIdDoesNotExistException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Userid does not exist!!! please check the url");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
	@ExceptionHandler(InvalidAddressIdException.class)
	public ResponseEntity<ErrorMessage> addressIdDoesNotExist(InvalidAddressIdException ex){
		ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Address id for the user: "+ex.getMessage()+" does not exist");
        return new ResponseEntity<>(error, HttpStatus.OK);
	}
	
}
