package com.infy.ekart.user.address.dto;

import com.infy.ekart.user.address.entity.AddressEntity;

public class AddressDTO {

	String address;
	String city;
	String state;
	Integer pincode;
	Long phoneNo;
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public Long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}

	public AddressDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AddressEntity createAddress(String userid) {
		AddressEntity address = new AddressEntity();
		address.setUserId(userid);
		address.setAddress(this.getAddress());
		address.setCity(this.getCity());
		address.setState(this.getState());
		address.setPincode(this.getPincode());
		address.setPhoneNo(this.getPhoneNo());
		
		return address;
	}
}
