package com.infy.ekart.user.address.exceptions;

public class InvalidPinCodeException extends Exception{

private static final long serialVersionUID = 1L;
	
	public InvalidPinCodeException() {
		super();
	}
	
	public InvalidPinCodeException(String error) {
		super(error);
	}
}
