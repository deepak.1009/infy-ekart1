************************INFY EKART************************

Initialization Steps (Setup) : 
_______________________________

1. Download the project from "source" folder at https://gitlab.com/deepak.2109/infy-ekart1.git

2. Please run the SQL Database script(attached with the project) to create necessary database schemas and table.

3. Change the username and password for MySQL server at https://gitlab.com/deepak.2109/infy-ekart1/-/blob/master/application.properties

========================================================================================================================================

NOTE : Zuul Gateway will run as as localhost:5000

THEMES IMPLEMENTED :
______________________

1. Account
===============
   
  1.1 Use Case(Sign Up) - As a buyer, I should be able to sign up for the application, so that I get the benefit of a registered buyer
      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/signup
      Body -raw -json
            {
    		"userEmailId" : "test.user@test.com",
    		"userName" : "Test User",
    		"password" : "Test@123"
            }
      
      Expected Result - User created successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages. 

   1.2 Use Case(Update Profile) - As a buyer, I should be able to modify details so that I can provide my new details.

      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userid>/update
      Body -raw -json
            {
    		"userName" : "Test User other",
                "oldPassword" : "Test@123",
                "newPassword" : "Test@125"
            }
      
      Expected Result - Username and password updated successfully!!!

   Note: Invalid requests are handled with meaningful Response messages.

2. Authentication
==================
   
  2.1 Use Case(Login) - As a user I should be able to login, so that I can use the services of ekart
      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/login
      Body -raw -json
            {
    		"userId" : "test.user@test.com",
    		"password" : "Test@125"
            }
      
      Expected Result - Login Successful. Welcome <userId>
      
   Note: Invalid requests are handled with meaningful Response messages.

3. Card Info
==================
   
  3.1 Use Case(Add Card) - As a registered buyer, I should be able to add new card which can be used while checking out 
                           so that I can avoid providing all the payment information again.
      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userId>/card/add
            <userId> = test.user@test.com

      Body -raw -json
            {
    		"cardNumber" : 1111222233334444,
    		"nameOnCard" : "Test User",
    		"expiryMonth" : 7,
   		"expiryYear" : 27
            }
      
      Expected Result - Card added successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages.

  3.2 Use Case(Delete Card) - As a registered buyer, I should be able to delete card, which I have already added
                              so that I can easily remove the already existing cards

      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userId>/card/<cardNumber>/delete
            <userId> = test.user@test.com
            <cardNumber> = a valid card number which is already added for this particular user.

      
      Expected Result - Card deleted successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages.


  3.3 Use Case(View Cards) - As a registered buyer, I should be able to view cards, which I have already added 

      
      Valid Request:
      ______________

      Method - GET
      url - localhost:5000/<userId>/cards
            <userId> = test.user@test.com

      
      Expected Result - All the cards will be displayed in Json format for the userId.
      
   Note: Invalid requests are handled with meaningful Response messages.


4. Address
==================
   
  4.1 Use Case(Add Address) - As a registered buyer, I should be able to add new shipping address which
                              can be used while checking out so that I can avoid typeing address everytime I purchase
      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userId>/address/add
            <userId> = test.user@test.com

      Body -raw -json
            {
    		"address" : "Test address 1",
    		"city" : "Test City",
    		"state" : "Maharashtra",
    		"pincode" : 111111,
    		"phoneNo" : 1122334455
            }
      
      Expected Result - Address added successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages.

  
   4.2 Use Case(Delete Address) - As a registered buyer, I should be able to delete the shipping addresses which I have already added 

      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userId>/address/<addressId>/delete
            <userId> = test.user@test.com
            <addressId> = a valid address id which is already added for this particular user.

      
      Expected Result - Card deleted successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages.


  4.3 Use Case(View Address to moodify) - As a registered buyer, I should be able to view the shipping address by address id which I have already added 

      
      Valid Request:
      ______________

      Method - GET
      url - localhost:5000/<userId>/address/<addressId>/view
            <userId> = test.user@test.com
            <addressId> = a valid address id which is already added for this particular user.

      
      Expected Result - Address for particular address id will be displayed in json format.
      
   Note: Invalid requests are handled with meaningful Response messages.


  4.4 Use Case(Modify Address) - As a registered buyer, I should be able to modify the shipping addresses
                                 which I have already added so that I can easily change the address to my new address
      
      Valid Request:
      ______________

      Method - POST
      url - localhost:5000/<userId>/address/<addressId>/modify
            <userId> = test.user@test.com
            <addressId> = a valid address id which is already added for this particular user.

      Body -raw -json
            {
    		"address" : "Test address 1",
    		"city" : "Test City1",
    		"state" : "Maharashtra",
    		"pincode" : 111111,
    		"phoneNo" : 1122334455
            }
      
      Expected Result - Address added successfully!!!
      
   Note: Invalid requests are handled with meaningful Response messages.

  4.5 Use Case(View Addresses) - As a registered buyer, I should be able to view the shipping addresses which I have already added 

      
      Valid Request:
      ______________

      Method - GET
      url - localhost:5000/<userId>/address/view
            <userId> = test.user@test.com

      
      Expected Result - Addresses for particular user id will be displayed in json format.
      
   Note: Invalid requests are handled with meaningful Response messages.

========================================================================================================================================

BASIC TROUBLESHOOTING

1. Error in console - Load balancer does not have available server for client: UserMS

Retry after 30-60 seconds as discovery server takes some time to refresh after a service is restarted.

2. Error in console - 

{
    "timestamp": "2021-02-23T14:08:57.804+0000",
    "status": 500,
    "error": "Internal Server Error",
    "message": "route:RibbonRoutingFilter"
}

Retry after 30 seconds as zuul server can take some time to refresh after a service is restrated
